# THIS IS A PROTOTYPE

That comes with many holes one can fall into if not careful.

## Compiling multiple files at once

This is very easy to forget, but compiling multiple files in the straight forward way results in
all the files having the same UID. I don't think I have any protection against this, so the only
symptom that appears is some or most chained functions not being called.

```bash

$ gcc $(sat-flags) -c *.c                           # Wrong!

$ for f in *.c ; do gcc $(sat-flags) -c $f ; done   # Right!

```

## Compiling files at all

I think I have a guard against this, but commented it out because it gave a false sense of security.
Basically you have to have a .uid file filled with something (like a 0) that sat-id can use as fuel
for its id generation muscles. This is again a workaround to enable the linking to restore what it
did when relinking, and be able to separate different files implementations from one another.
Forgetting this or getting this wrong might lead to a partly running program or a linking error.

```bash

$ #   vvvvvvvvvvvv---------------- This is a little helper that defines the C define using sat-id
$ gcc $(sat-flags) -c a_file.c

```

## This version has a weird curlies quirk

Yes. As seen above, a when(..), after(...) or before(...) should NOT be followed with a opening curly.
This is because of the implicit return functionality, that makes the return statment always return
the first argument or 0 if there is not first argument. Embrace the non-perfectness of a prototype.
The alternative is to always force the user to explicitly return the first argument. Possibly clearer,
but looks like a mess when we don't change the value, but instead just use it. In the next version
implicit returns will handle this automatically, and returning a value will simply override that
behaviour. It will be gracious.

```c
  #include <stdio.h>
  #include <sat/impl.h>

  when(creating_things)                           // No opening '{'
    printf("We are creating things!!\n");
    return;                                       // This return is a macro in disguise
  }
  
```

## The return thingie messes up ordinary functions

Since we cannot run any macro code *after* the function body, there is no possibility of restraining
this return macro to only the function definition. That means that if we have ordinary function or
include header files that define functions with explicit returns, none of those will compile properly.
It aint pretty. But it is a prototype. Make sure to have the `#include <sat/impl.h>` at the bottom, or
be careful and wrap other includes and function definitions inside `#include <sat/noimpl.h>` and
`#include <sat/impl.h>` to make the return macro temporarily go away.

```c
  #include <sat/impl.h>
  #include <stdio.h>        // Some libraries will crumble doing this, some will be happy enough

  static int get_a_number()
  {
    return 42;              // This will not work! Move above the <sat/impl.h> or use <sat/noimpl.h>
  }

  when(creating_things)
    printf("We are creating things!!\n");
    return;
  }
  
```

## The return thingie is obligatory

It is what enables the function to continue the chain. It is thus needed in void->void functions as 
well as functions with parameters. I suggest to use -Wreturn-type as a protection against getting
this wrong. The plan is to add that to `sat-flags` but I am a tad forgetful.Previously I returned 
void when there was no first argument, but as a workaround to make -Wreturn-type help out I changed 
this to return an int, that is always 0. Getting this wrong leads to partially functioning links, 
where the chain only computes until the function without an explicit return - at which point it 
returns instead of continuing the chain.

```c
  #include <sat/impl.h>

  when(getting_a_two, int, x)
    x = 2;
    return;             // Send the 2 along the chain (or return if last in the chain)
  }

  static int my_number;

  when(doing_work)
    my_number = 100;
    return;             // Run the next in the chain (if not last), this returns a 0 behind the scenes
  }
  
```

## Forgetting the automatic return of the first argument

This can lead to ugly code. In the last version it was very intuitive to use sat_get and receive
a value to directly use. The exact same is possible in the new version as well.

```c
  #include <sat/decl.h>

  sat(create_a_thing, thing_t *);

  #include <sat/impl.h>

  thing_t fallback = { 0 };
  position_t my_position = { 50, 25 };

  when(creating_things)
    create_a_thing(&fallback)->points_at = my_position; // No need to keep the actual pointer around
    return;
  }
  
```

