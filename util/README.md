# Satellite util directory

This version of satellite is a group of scripts written in C.
The route to get redefines for a specific set of object files is:

```sh
    $ objdump -t $object_files
    | util/grep_filenames_and_sat_functions.c
    | util/filename_every_line.c
    | column -t -s : -o : | LC_ALL=C sort -fs | sed -E 's/ +:/:/g'
    | util/which_symbol_redefines.c
    | column -t -s : -o : | sort | sed -E 's/ +:/:/'
    | util/unify_sorted_redefines.c
```

The redefines are for use with a patched version of `objcopy` where
`--redefine-defined-sym old=new` makes `objcopy` redefine only the
symbols which have function definitions. This is necessary to be 
able to be able to both add and remove hooks from all function calls,
without the patch some function calls would be renamed along with a
function that prior to the current sync was the entry point to the 
hook chain. The redefines come with specific file names for which
they should be applied. They can not be all bundled together and
applied to all files, since some entry point symbols are to be
redefined to other symbols than others, and these are file specific.

All scripts are based on reading and processing line by line, and
not using any hash tables or complex storage. This is achieved
by using external sorting between the commands, meaning that basic
buffers and forget-as-you-go is a valid and fast approach within
the scripts.

### `grep_filenames_and_sat_functions.c | filename_every_line.c`

These scripts filters out the relevant information of the `objdump -t`
output and puts the filename after every symbol so that they can
be sorted by symbol without losing information on file location.

### `which_symbol_redefines.c`

This script handles the hooking logic. It works on a per satellite
symbol basis; first adding lines in a buffer for every redefine
information symbol `sat_SYMBOL_TYPE_FROM_x_TO`, then inserting
information like `TO` type, number of `default`, `before`, `when`
and `after` functions, and finally syncing `default` functions if
there are no implementations, otherwise desyncing any used `default`
and syncing the implementations starting from the first and earliest
entry point and working its way from there.

### `which_symbol_redefines.c`

This script just aggregates the redefines from `which_symbol_redefines`
on a per file basis so that as few `objcopy` calls as possible are made
for those redefines.

