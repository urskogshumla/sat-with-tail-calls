#!/usr/bin/env -S @tcc@/bin/tcc -run

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main(int argc, const char *argv[])
{
    size_t line_length = 1024;
    char *line_buffer = malloc(sizeof(char) * line_length);
    *line_buffer = '\0';
    size_t file_length = 1024;
    char *file_buffer = malloc(sizeof(char) * file_length);
    *file_buffer = '\0';
    while (getline(&line_buffer, &line_length, stdin) != -1)
    {
        char *c = line_buffer;
        while (*c != '\n') c++;
        *c = '\0';

        if (*line_buffer == ':')
            strncpy(file_buffer, line_buffer + 1, file_length);
        else if (*line_buffer == '-')
            printf("%s:%c:-\n", line_buffer + 1, *line_buffer);
        else
            printf("%s:%c:%s\n", line_buffer + 1, *line_buffer, file_buffer);
    }
}

