#!/usr/bin/env -S @tcc@/bin/tcc -run

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

static void swap(char **a, char **b)
{
    char *c = *b;
    *b = *a;
    *a = c;
}

int main(int argc, const char *argv[])
{
    size_t line_length = 1024;
    char *current_line = malloc(sizeof(char) * line_length);
    *current_line = '\0';
    char *last_line = malloc(sizeof(char) * line_length);
    *last_line = '\0';

    char *last_name = "";
    char last_type = '\0';
    char *last_file = "";
    char needs_default = 1;

    char first = 1;

    while (getline(&current_line, &line_length, stdin) != -1)
    {
        char *name = current_line;
        char *c = name;
        while (*++c != ':');
        // skip spaces in first field
        {
            char *e = c;
            while (*--e == ' ' || *e == '\t');
            e[1] = '\0';
        }
        char type = *++c;
        c += 2;
        char *file = c;
        while (*c++ != '\n');
        c[-1] = '\0';
        // printf("\nLast '%s', type '%c', file '%s'\n", last_name, last_type, last_file);
        // printf("Name '%s', type '%c', file '%s'\n", name, type, file);

        if (strcmp(name, last_name) != 0)
        {
            if (first) first = 0;
            else if (needs_default)
            {
                if (last_type == '>') printf("%s:--redefine-defined-sym __esa_start_esa_%s=__start_esa_%s "
                                             "--redefine-defined-sym __esa_stop_esa_%s=__stop_esa_%s\n",
                                             last_file, last_name, last_name,
                                             last_name, last_name);
            }
            else needs_default = 1;
        }
        if (type == '-')
            needs_default = 0;
        else if (type == '<')
        {
            if (needs_default) needs_default = 0;
            else printf("%s:--redefine-defined-sym __start_esa_%s=__esa_start_esa_%s "
                        "--redefine-defined-sym __stop_esa_%s=__esa_stop_esa_%s\n",
                        file, name, name,
                        name, name);
        }
        swap(&current_line, &last_line);
        last_type = type;
        last_name = name;
        last_file = file;
    }
    if (needs_default && last_type == '>') printf("%s:--redefine-defined-sym __esa_start_esa_%s=__start_esa_%s "
                "--redefine-defined-sym __esa_stop_esa_%s=__stop_esa_%s\n",
                last_file, last_name, last_name,
                last_name, last_name);
    return 0;
}

