#!/usr/bin/env -S @tcc@/bin/tcc -run

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

static int  is_num_or_let(char c)
{
    return (
        (c >= '0' && c <= '9')
        || (c >= 'a' && c <= 'z')
        || (c >= 'A' && c <= 'Z')
    );
}

static int is_hex(char c)
{
    return ((c >= '0' && c <= '9') || (c >= 'a' && c <= 'f'));
}

int main(int argc, const char *argv[])
{
    size_t line_length = 1024;
    char *line_buffer= malloc(sizeof(char) * line_length);
    *line_buffer= '\0';
    char printed = 0;

    while (getline(&line_buffer, &line_length, stdin) != -1)
    {
        if (*line_buffer == EOF) break;
        if (*line_buffer == '\n') continue;
        if (strncmp("SYMBOL TABLE", line_buffer, strlen("SYMBOL_TABLE")) == 0) continue;

        // Check if line is a filename and print the filename if that is the case
        {
            char *c, *maybe_filename_end, swap;

            c = line_buffer;

            while (*++c != ' ');
            maybe_filename_end = c - 1;
            swap = *maybe_filename_end;
            *maybe_filename_end = '\0';

            while (*++c == ' ');

            if (strncmp("file format", c, strlen("file_format")) == 0)
            {
                printf(":%s\n", line_buffer);
                continue;
            }
            *maybe_filename_end = swap;
        }

        // Check symbol line
        {
            char *c, *section_name, is_default;
            
            is_default = 0;
            c = line_buffer;

            while (*++c != ' ' && *c != '\t');
            c += 8 + 1;
            section_name = c;
            while (*++c != ' ' && *c != '\t');
            *c = '\0';
            if (strncmp("esa_", section_name, strlen("esa_")) == 0)
            {
                printf("-%s\n", section_name + strlen("esa_"));
                continue;
            }
            else if (strncmp(".rodata", section_name, strlen(".rodata")) != 0)
                continue;

            while (*++c == ' ');
            while (*++c != ' '); // address 2
            c++;
            if (strncmp("__esa_start_esa_", c, strlen("__esa_start_esa_")) == 0)
            {
                printf(">%s", c + strlen("__esa_start_esa_"));
                continue;
            }
            else if (strncmp("__start_esa_", c, strlen("__start_esa_")) == 0)
            {
                printf("<%s", c + strlen("__start_esa_"));
                continue;
            }
        }
    }
    free(line_buffer);
    return 0;
}

