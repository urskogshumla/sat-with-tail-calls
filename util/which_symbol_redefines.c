#!/usr/bin/env -S @tcc@/bin/tcc -run

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

// This currently requires a patched objcopy.

#define MAX_LINES (512) // This is the maximum amount of hooks that can implement the same sat function 

typedef struct
{
    struct { struct { char uid_id[128 - 1], type; } identity, connecting_back_as; } current, target;
    char should_redefine_connecting_as, should_redefine_uid, file[128 - 2];
} Line;

static int is_num_or_let(char c)
{
    return (
        (c >= '0' && c <= '9')
        || (c >= 'a' && c <= 'z')
        || (c >= 'A' && c <= 'Z')
    );
}

static int is_num_or_let_or_underscore(char c)
{
    return (is_num_or_let(c) || c == '_');
}

static void swap_lines(Line *lines, const int a, const int b)
{
    Line _ = lines[a];
    lines[a] = lines[b];
    lines[b] = _;
}

static void print_symbol_redefines(Line *lines, const int line_count, const char *name, const int d_count, const int b_count, const int w_count, const int a_count)
{
    __label__ entry_candidate_checked;
    int lines_left = line_count;
    Line *line_slice = lines;

    char *entry = "___";
    char entry_type = 'e';

    int impl_count = b_count + w_count + a_count;
    int finding_default = (!impl_count);

    int d_left = d_count;
    for (int i = 0 ; i < d_left ; i++)
        if (finding_default)
        {
            if (strcmp(line_slice[i].target.connecting_back_as.uid_id, "___") == 0) finding_default = 0; // USE ALREADY CONNECTED DEFAULT
            else if (finding_default && !--d_left) // CONNECT LAST DEFAULT
            {
                line_slice[i].target.connecting_back_as.type = 'e';
                strcpy(line_slice[i].target.connecting_back_as.uid_id, "___");
                line_slice[i].should_redefine_connecting_as = 1;
                break;
            }
        }
        else if (strcmp(line_slice[i].target.connecting_back_as.uid_id, "___") == 0) // DISCONNECT UNUSED DEFAULT
        {
            line_slice[i].target.connecting_back_as.type = line_slice[i].target.identity.type - ('a' - 'A');
            strcpy(line_slice[i].target.connecting_back_as.uid_id, line_slice[i].target.identity.uid_id);
            line_slice[i].should_redefine_connecting_as = 1;
        }

    entry = "___";
    entry_type = 'e';

    line_slice += d_count;
    lines_left -= d_count;

    // FIXME go to beginning of each count and find each entry and swap longest one to beginning

    for ( ; lines_left ; lines_left--)
    {
        int candidate_search_range = (b_count ? b_count : (w_count ? w_count : a_count)); 
        for (int i = 0 ; i < candidate_search_range ; i++)
            if (strcmp(line_slice[i].target.connecting_back_as.uid_id, entry) == 0) { swap_lines(line_slice, 0, i); goto entry_candidate_checked; }
        line_slice[0].target.connecting_back_as.type = entry_type;
        strcpy(line_slice[0].target.connecting_back_as.uid_id, entry);
        line_slice[0].should_redefine_connecting_as = 1;
        entry_candidate_checked:
        entry = line_slice[0].target.identity.uid_id;
        entry_type = line_slice[0].target.identity.type;

        // FIXME move these upwards? to begin checking whens at last before and afters at last when?
        if (b_count) b_count--;
        else if (w_count) w_count--;
        else a_count--;

        line_slice++;
    }


    for (int i = 0 ; i < line_count; i++)
    {
        if (!lines[i].should_redefine_connecting_as && !lines[i].should_redefine_uid) continue;

        printf(
            "%s:"

            " --redefine-defined-sym __sat_%s_%c_%s"
            "=__sat_%s_%c_%s"

            " --redefine-defined-sym __sat_%s_%c_%s_x_%s"
            "=__sat_%s_%c_%s_x_%s",

            lines[i].file,

            name, lines[i].current.connecting_back_as.type, lines[i].current.connecting_back_as.uid_id,
            name, lines[i].target.connecting_back_as.type, lines[i].target.connecting_back_as.uid_id,

            name, lines[i].current.identity.type - ('a' - 'A'), lines[i].current.identity.uid_id, lines[i].current.connecting_back_as.uid_id,
            name, lines[i].target.identity.type - ('a' - 'A'), lines[i].target.identity.uid_id, lines[i].target.connecting_back_as.uid_id);

        if (lines[i].should_redefine_uid)
        {
            printf(
                " --redefine-defined-sym __sat_%s_%c_%s"
                "=__sat_%s_%c_%s",

                name, lines[i].current.identity.type, lines[i].current.identity.uid_id,
                name, lines[i].target.identity.type, lines[i].target.identity.uid_id);
        }

        printf("\n");
    }
}

int main(int argc, const char *argv[])
{
    /*
        Input is lines of the following formats (spaces added for clarity):

            NAME : -          : [-<=>] : IDENTITY_UID_ID : REDEFINED_UID_ID : [-n] : FILENAME
            NAME : [DdBbWwAa] : [fi]   : UID_ID          : -                : [-n] : FILENAME

        where

            NAME              is the satellite symbol name.

            [-]               is the information type, where '-' are conversions, as many
                              as there are definitions. Those are used to store the lines
            [DdBbWwAa]        necessary to output redefines, while the other types are
                              used to complement the info in every conversion.

            [-<=>]            is the definition type that a conversion's information
            .                 is regarding, '-' is 'default', '<' is 'before', '='
            [fi]              is 'when' and '>' is 'after'. For complements (DdBbWwAa)
                              'f' describes 'forward', a function call to the next
                              definition in the chain, and 'i' describes 'implementation',
                              a function definition that gets called by code or the
                              previous definition in the chain.

            IDENTITY_UID_ID     is the identity uid_id of the conversion or the uid_id of the
            UID_ID            current information complementation. This is ONLY guaranteed
                              to be the uid_id that the conversion or definition is
                              currently defined as IF the [-n] field is '-' for that line.
                              Otherwise the uid_id is IDENTITY_UID_ID with '0' as UID.

            REDEFINED_UID_ID  is only used for conversions and describes the uid_id that
                              the 'implementation' function definition is currently defined
                              as. This is ONLY guaranteed IF the [-n] field is '-' for that
                              line. Otherwise the uid_id is REDEFINED_UID_ID with '0' as UID.

            [-n]              is the identity uid generation status; '-' means nothing has 
                              been generated, 'n' means that this line's uid:s have been
                              switched to fresh ones and are actually currently defined with
                              '0' as uid. The ones found in implementation are '0'.

            FILENAME          is the name of the file that this conversion or complement is
                              coming from.
    */

    int d_count       = 0,
        b_count       = 0,
        w_count       = 0,
        a_count       = 0;

    Line lines[MAX_LINES]; // Initialize to zero? 
    int line_count = 0;

    size_t line_length = 1024;
    char *line_buffer = malloc(sizeof(char) * line_length);
    *line_buffer = '\0';

    char *c, current_name[256] = { '\0' };

    char
       *field_1_NAME,                        
        field_2_dash_or_DdBbWwAa,            
        field_3_defaultbeforewhenafter_or_fi,
       *field_4_IDENTITY_UID_ID_or_UID_ID,     
       *field_5_dash_or_REDEFINED_UID_ID,    
    //    field_6_dash_or_n,                   
       *field_7_FILE;                        

    while (getline(&line_buffer, &line_length, stdin) != -1)
    {
        c = line_buffer;

        field_1_NAME                          =  c ; while (is_num_or_let_or_underscore(*++c)) ; *c++ = '\0';
        field_2_dash_or_DdBbWwAa              = *c ; c += 2;
        field_3_defaultbeforewhenafter_or_fi  = *c ; c += 2;
        field_4_IDENTITY_UID_ID_or_UID_ID       =  c ; while (is_num_or_let_or_underscore(*++c)) ; *c++ = '\0';
        field_5_dash_or_REDEFINED_UID_ID               =  c ; if (*c == '-') c++ ; else while (is_num_or_let_or_underscore(*++c)) ; *c++ = '\0';
        // field_6_dash_or_n                     = *c ; c += 2;
        field_7_FILE                          =  c ; while (*++c != '\n') ; *c = '\0';

        switch (field_3_defaultbeforewhenafter_or_fi)
        {
            case '-': field_3_defaultbeforewhenafter_or_fi = 'd'; break;
            case '<': field_3_defaultbeforewhenafter_or_fi = 'b'; break;
            case '=': field_3_defaultbeforewhenafter_or_fi = 'w'; break;
            case '>': field_3_defaultbeforewhenafter_or_fi = 'a'; break;
        }

        if (*current_name == '\0')
            strcpy(current_name, field_1_NAME);
        else if (strcmp(current_name, field_1_NAME) != 0)
        {
            print_symbol_redefines(lines, line_count, current_name, d_count, b_count, w_count, a_count);
            strcpy(current_name, field_1_NAME);
            d_count       = 0;
            b_count       = 0;
            w_count       = 0;
            a_count       = 0;
            line_count    = 0;
        }

        if (field_2_dash_or_DdBbWwAa == '-')
        {
            // set file
            strcpy(lines[line_count].file, field_7_FILE);

            // initially set 'should_redefine':s
            lines[line_count].should_redefine_connecting_as = 0;
            lines[line_count].should_redefine_uid = 0;
        
            // set target identity and connecting_back_as
            strcpy(lines[line_count].target.identity.uid_id, field_4_IDENTITY_UID_ID_or_UID_ID);
            lines[line_count].target.identity.type = field_3_defaultbeforewhenafter_or_fi;
            strcpy(lines[line_count].target.connecting_back_as.uid_id, field_5_dash_or_REDEFINED_UID_ID);
            lines[line_count].target.connecting_back_as.type = 'e';

            // manipulate identity uid_id (field 4 and 5) for current if generate_uids has altered it
            // if (field_6_dash_or_n == 'n')
            // {
            //     while (*(++(field_4_IDENTITY_UID_ID_or_UID_ID) + 1) != '_');
            //     *field_4_IDENTITY_UID_ID_or_UID_ID = '0';
            //     while (*(++(field_5_dash_or_REDEFINED_UID_ID) + 1) != '_');
            //     *field_5_dash_or_REDEFINED_UID_ID = '0';
            //     lines[line_count].should_redefine_uid = 1;
            // }
            // set current identity and connecting_back_as
            strcpy(lines[line_count].current.identity.uid_id, field_4_IDENTITY_UID_ID_or_UID_ID);
            lines[line_count].current.identity.type = field_3_defaultbeforewhenafter_or_fi;
            strcpy(lines[line_count].current.connecting_back_as.uid_id, field_5_dash_or_REDEFINED_UID_ID);
            lines[line_count].current.connecting_back_as.type = 'e';

            line_count++;
        }
        else if (field_3_defaultbeforewhenafter_or_fi == 'f')
            switch (field_2_dash_or_DdBbWwAa)
            {
                case 'b': b_count++ ; break;
                case 'w': w_count++ ; break;
                case 'a': a_count++ ; break;
                case 'd': d_count++ ; break;
            }
        else if (field_3_defaultbeforewhenafter_or_fi == 'i')
            for (int i = 0 ; i < line_count ; i++)
                if (strcmp(field_4_IDENTITY_UID_ID_or_UID_ID, lines[i].target.connecting_back_as.uid_id) == 0)
                {
                    lines[i].target.connecting_back_as.type = field_2_dash_or_DdBbWwAa;
                    lines[i].current.connecting_back_as.type = field_2_dash_or_DdBbWwAa;
                }
    }
    print_symbol_redefines(lines, line_count, current_name, d_count, b_count, w_count, a_count);
}

