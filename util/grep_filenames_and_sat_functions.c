#!/usr/bin/env -S @tcc@/bin/tcc -run

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

static void move_and_insert_char(const int l, char *s, char c)
{
    int i = l - 2;
    while (i--) s[i + 2] = s[i];
    s[0] = c;
    s[1] = ':';
}

static void erase_first_2_chars(char *s)
{
    int length = strlen(s);
    for (int i = 2; i < length; i++)
        s[i - 2] = s[i];
    s[length - 2] = '\0';
}

static int  is_num_or_let(char c)
{
    return (
        (c >= '0' && c <= '9')
        || (c >= 'a' && c <= 'z')
        || (c >= 'A' && c <= 'Z')
    );
}

static int is_hex(char c)
{
    return ((c >= '0' && c <= '9') || (c >= 'a' && c <= 'f'));
}

int main(int argc, const char *argv[])
{
    size_t line_length = 1024;
    char *line_buffer= malloc(sizeof(char) * line_length);
    *line_buffer= '\0';
    char printed = 1;

    while (getline(&line_buffer, &line_length, stdin) != -1)
    {
        if (*line_buffer == EOF) break;
        if (*line_buffer == '\n') continue;
        int i = 0;
        while (i < 16 && is_hex(line_buffer[i])) i++;
        if (i == 16 && line_buffer[i] == ' ')
        {
            char impl_type = '-';
            char *c = line_buffer;

            while (*c != ' ') c++;
            c++;
            if (*c == 'g') impl_type = 'i';
            else if (*(++c) == 'w') impl_type = 'f';
            else continue; // *UND*
            while (*c == ' ') c++;
            while (*c != '*' && *c != '.') c++;
            while (*c != ' ') c++;
            while (*c == ' ') c++;

            if (strncmp("__sat_", c, strlen("__sat_")) != 0) continue;

            char is_informational = 0;
            {
                char *c = line_buffer + strlen(line_buffer) - 1;
                *c-- = '\0';
                if (*c == '_') c--; else while (is_num_or_let(*c)) c--; // uid
                c--;
                if (*c == '_') c--; else while (is_num_or_let(*c)) c--; // id
                *c-- = ':';
                char *type = c;
                while (is_num_or_let(*c)) c--; // type
                *c-- = ':';
                if (*type == 'x')
                {
                    while (is_num_or_let(*c)) c--; // uid
                    c--;
                    while (is_num_or_let(*c)) c--; // id
                    *c = ':';
                    c -= 2;
                    *c++ = ':';
                    move_and_insert_char(type - c + 1, c, '-');
                    c += 2;
                    switch (*c) // Used for sorting the _x_ pairs
                    {
                        case 'B': *c = '<' ; break;
                        case 'W': *c = '=' ; break;
                        case 'A': *c = '>' ; break;
                        case 'D': *c = '-' ; break;
                        default:
                            printf("Error, is at '%s'\n", c);
                            break;
                    }
                    is_informational = 1;
                }
                else
                {
                    char *move_part = type + 2;
                    int move_part_length = strlen(move_part) + 2;
                    move_part[move_part_length] = '\0';
                    move_and_insert_char(move_part_length, move_part, impl_type);
                    int length = strlen(line_buffer);
                    if (*type == 'e')
                    {
                        // line_buffer[length - 3] = '-';
                        // line_buffer[length - 2] = '\0';
                        // length -= 2;
                    }
                    strcpy(line_buffer + length, ":-");
                }
            }

            printf("    %s\n", c + strlen("__sat_"));
            printed = 1;
            continue;
        }
        {
            char *c = line_buffer;
            while (*c != ':') c++;
            *c = '\0';
        }

        if (strcmp("SYMBOL TABLE", line_buffer) == 0) continue;

        if (printed) printf(":%s\n", line_buffer);
        continue;
    }
    free(line_buffer);
}

