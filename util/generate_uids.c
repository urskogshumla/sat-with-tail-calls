#!/usr/bin/env -S @tcc@/bin/tcc -run

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define uid_buffer_length            (1024)
#define file_buffer_length           (1024)

static char
    uid_buffer[uid_buffer_length],
    file_buffer[file_buffer_length],
    is_first_sym = 1,
    has_read_file_uid = 0,
    new_uid_indicator = '-',
    *new_uid_buffer,
    *line_buffer,
    *new_uid;
static size_t
    new_uid_buffer_length = 1024,
    line_buffer_length = 1024;
static const char
    *uid_file_name;
static FILE
    *uid_file_handle;

static inline void open_and_read_uid_file()
{
    new_uid_buffer = malloc(sizeof(char) * new_uid_buffer_length);
    uid_file_handle = fopen(uid_file_name, "r+");
    if (!uid_file_handle)
    {
        fprintf(stderr, "ERROR: Failed to open uid file '%s'.\n", uid_file_name);
        exit(1);
    }
    int c, line_length = 0;
    do c = fgetc( uid_file_handle );
    while ((c != EOF && c != '\n') && ++line_length);
    if (!line_length)
    {
        fprintf(stderr, "ERROR: No value found in uid file '%s'.\n", uid_file_name);
        exit(1);
    }
    new_uid = uid_buffer + uid_buffer_length - line_length - 1;
    rewind(uid_file_handle);
    fread(new_uid, 1, line_length, uid_file_handle);
}

static inline void write_and_close_uid_file()
{
    rewind(uid_file_handle);
    fwrite(new_uid, strlen(new_uid), 1, uid_file_handle);
    fclose(uid_file_handle);
}

static inline void generate_new_uid()
{
    __label__ done;
    int i = strlen(new_uid);
    while (i-- >= 0)
    {
        new_uid[i]++;
        switch (new_uid[i]) 
        {
            case 'Z' + 1:
                new_uid[i] = '0';
                continue;
            case 'z' + 1:
                new_uid[i] = 'A';
                goto done;
            case '9' + 1:
                new_uid[i] = 'a';
                goto done;
            default:
                goto done;
        }
    }
    *new_uid = '0';
    new_uid = new_uid - 1;
    *new_uid = '1';

    done:
    strcpy(uid_buffer, new_uid);
}

int main(int argc, const char *argv[])
{
    if (argc <= 1)
    {
        fprintf(stderr, "ERROR: No uid file specified as argument to %s.\n", argv[0]);
        exit(1);
    }
    uid_file_name = argv[1];

    line_buffer = malloc(sizeof(char) * line_buffer_length);

    while (getline(&line_buffer, &line_buffer_length, stdin) != -1)
    {

        if (*line_buffer == ' ')
        {
            char *c = line_buffer + strlen(line_buffer) - 1;
            *c = '\0';

            char const *first_uid, *first_id, *second_uid, *second_id;

            int second_is_unused = 0;

            if (c[-1] == '_')
            {
                printf("%s:%c\n", line_buffer, '-');
                continue;
            }
            if (*--c == '-')
            {
                c -= 1;
                *c = '\0';
                second_is_unused = 1;
            }
            else
            {
                while (*--c != '_');
                *c = '\0';
                second_id = c + 1;

                while (*--c != ':');
                *c = '\0';
                second_uid = c + 1;
            }

            while (*--c != '_');
            *c = '\0';
            first_id = c + 1;

            while (*--c != ':');
            *c = '\0';
            first_uid = c + 1;

            if (is_first_sym)
            {
                if (strcmp(first_uid, "0") != 0)
                {
                    new_uid_indicator = '-';
                    strcpy(uid_buffer, first_uid);
                }
                else
                {
                    new_uid_indicator = 'n';
                    if (!has_read_file_uid)
                    {
                        open_and_read_uid_file();
                        has_read_file_uid = 1;
                    }
                    generate_new_uid();
                }
                is_first_sym = 0;
            }
            if (second_is_unused)
            {
                printf("%s:%s_%s:-:%c\n", line_buffer, uid_buffer, first_id, new_uid_indicator);
                continue;
            }
            printf("%s:%s_%s:%s_%s:%c\n", line_buffer, uid_buffer, first_id, uid_buffer, second_id, new_uid_indicator);
        }
        else if (*line_buffer == ':')
        {
            printf("%s", line_buffer);
            is_first_sym = 1;
        }
        else printf("ERROR: %s", line_buffer);
    }
    if (has_read_file_uid)
    {
        write_and_close_uid_file();
        has_read_file_uid = 1;
    }
}

