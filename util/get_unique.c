#!/usr/bin/env -S @tcc@/bin/tcc -run

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define BUFFER_SIZE (1024)
static char line_buffer[BUFFER_SIZE];

int main(int argc, const char *argv[])
{
    char *input = line_buffer + 1;
    size_t line_length = BUFFER_SIZE - 1;
    FILE *f = fopen((argc > 1) ? argv[1] : ".uid", "r+");
    if (!f)
        goto just_error;
    else if (getline(&input, &line_length, f) == -1)
        goto close_file_and_error;
    char before = 0;
    int i = strlen(input);
    if (input[i - 1] == '\n') i--;
    while (i--)
    {
        input[i]++;
        switch (input[i]) 
        {
            case 'Z' + 1:
                input[i] = '0';
                continue;
            case 'z' + 1:
                input[i] = 'A';
                goto done;
            case '9' + 1:
                input[i] = 'a';
                goto done;
            default:
                goto done;
        }
    }
    *input = '0';
    *line_buffer = '1';
    input = line_buffer;
done:
    printf("%s", input);
    rewind(f);
    fwrite(input, strlen(input), 1, f);
    fclose(f);
    return 0;
close_file_and_error:
    fclose(f);
just_error:
    printf("UID_ERROR");
    return 1;
}

