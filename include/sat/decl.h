#include "sat_partials/_guard_uid.c"

#ifndef __SAT_DECL__
#define __SAT_DECL__

#   ifdef __SAT_IMPL__
#   include "noimpl.h"
#   endif

#   include "sat_partials/_decl.h"

#   define sat(  NAME, ARGS... ) _sat_decl(  NAME, ##ARGS )

#endif

