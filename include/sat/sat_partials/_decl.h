#ifndef __SAT_PARTIAL_CALL
#define __SAT_PARTIAL_CALL

#   include "_common.h"

#   define _sat_call( NAME, ARGS... ) _sat_call_redir( NAME, _sat_first_or_void( "ignored", ##ARGS ), ##ARGS )
#   define _sat_call_redir( NAME, A, ARGS... )\
        ({\
            A __sat_##NAME( _sat_types( "ignored", ##ARGS ));\
            __sat_##NAME( _sat_values( "ignored", ##ARGS ));\
        })

#   define _sat_decl(                 NAME, ARGS...    ) _sat_decl_redir(  __COUNTER__ , SAT_UID, NAME, ##ARGS                                   )
#   define _sat_decl_redir(  ID, UID, NAME, ARGS...    ) _sat_decl_unique( ID,           UID,     NAME, _sat_first_or_void( ID, ##ARGS ), ##ARGS )
#   define _sat_decl_unique( ID, UID, NAME, A, ARGS... )\
        _SAT_WEAK void __sat_##NAME##_D_##UID##_##ID##_x_##UID##_##ID() {};\
        _SAT_WEAK A __sat_##NAME##_d_##UID##_##ID( _sat_as_parameters( "ignored", ##ARGS )) {\
            _sat_nothing_or_x1( "ignored", ##ARGS )\
        };\
        A __sat_##NAME##_D_##UID##_##ID(_sat_as_parameters( ID, ##ARGS )) {\
            return __sat_##NAME##_d_##UID##_##ID(_sat_as_arguments( ID, ##ARGS ));\
        };\
        A __sat_##NAME##_e____( _sat_as_parameters( "ignored", ##ARGS ));\
        _SAT_INLINE A NAME( _sat_as_parameters( "ignored", ##ARGS ))\
            {\
                return __sat_##NAME##_e____( _sat_as_arguments( "ignored", ##ARGS ) );\
            }

#   define _sat_nothing_or_x1( _, ARGS... ) _sat_GET_NTH_ARG( "ignored", ##ARGS, _sat_nothing_or_x1_pos, _sat_nothing_or_x1_pos, _sat_nothing_or_x1_pos, _sat_nothing_or_x1_pos, _sat_nothing_or_x1_pos, _sat_nothing_or_x1_pos, _sat_nothing_or_x1_pos, _sat_nothing_or_x1_pos, _sat_nothing_or_x1_pos, _sat_nothing_or_x1_pos, _sat_nothing_or_x1_000 ) ( ID, ##ARGS )
#   define _sat_types(         _, ARGS... ) _sat_GET_NTH_ARG( "ignored", ##ARGS, _sat_types_010,         _sat_is_odd,            _sat_types_008,         _sat_is_odd,            _sat_types_006,         _sat_is_odd,            _sat_types_004,         _sat_is_odd,            _sat_types_002,         _sat_is_odd,            _sat_types_000         ) ( "ignored", ##ARGS )
#   define _sat_as_parameters( _, ARGS... ) _sat_GET_NTH_ARG( "ignored", ##ARGS, _sat_as_parameters_010, _sat_as_parameters_009, _sat_as_parameters_008, _sat_as_parameters_007, _sat_as_parameters_006, _sat_as_parameters_005, _sat_as_parameters_004, _sat_as_parameters_003, _sat_as_parameters_002, _sat_as_parameters_001, _sat_as_parameters_000 ) ( "ignored", ##ARGS )
#   define _sat_as_arguments(  _, ARGS... ) _sat_GET_NTH_ARG( "ignored", ##ARGS, _sat_as_arguments_010,  _sat_as_arguments_009,  _sat_as_arguments_008,  _sat_as_arguments_007,  _sat_as_arguments_006,  _sat_as_arguments_005,  _sat_as_arguments_004,  _sat_as_arguments_003,  _sat_as_arguments_002,  _sat_as_arguments_001,  _sat_as_arguments_000  ) ( "ignored", ##ARGS )

#   define _sat_nothing_or_x1_000(                                             ... ) return 0;
#   define _sat_nothing_or_x1_pos( _, a1,                                      ... ) return x1;
#   define _sat_types_000(                                                     ... ) void
#   define _sat_types_002(         _, A1, a1,                                  ... ) A1
#   define _sat_types_004(         _, A1, a1, A2, a2,                          ... ) A1, A2
#   define _sat_types_006(         _, A1, a1, A2, a2, A3, a3,                  ... ) A1, A2, A3
#   define _sat_types_008(         _, A1, a1, A2, a2, A3, a3, A4, a4,          ... ) A1, A2, A3, A4
#   define _sat_types_010(         _, A1, a1, A2, a2, A3, a3, A4, a4, A5, a5,  ... ) A1, A2, A3, A4, A5
#   define _sat_as_parameters_000(                                             ... ) 
#   define _sat_as_parameters_001( _, a1,                                      ... ) a1 x1
#   define _sat_as_parameters_002( _, a1, a2,                                  ... ) a1 x1, a2 x2
#   define _sat_as_parameters_003( _, a1, a2, a3,                              ... ) a1 x1, a2 x2, a3 x3
#   define _sat_as_parameters_004( _, a1, a2, a3, a4,                          ... ) a1 x1, a2 x2, a3 x3, a4 x4
#   define _sat_as_parameters_005( _, a1, a2, a3, a4, a5,                      ... ) a1 x1, a2 x2, a3 x3, a4 x4, a5 x5
#   define _sat_as_parameters_006( _, a1, a2, a3, a4, a5, a6,                  ... ) a1 x1, a2 x2, a3 x3, a4 x4, a5 x5, a6 x6
#   define _sat_as_parameters_007( _, a1, a2, a3, a4, a5, a6, a7,              ... ) a1 x1, a2 x2, a3 x3, a4 x4, a5 x5, a6 x6, a7 x7
#   define _sat_as_parameters_008( _, a1, a2, a3, a4, a5, a6, a7, a8,          ... ) a1 x1, a2 x2, a3 x3, a4 x4, a5 x5, a6 x6, a7 x7, a8 x8
#   define _sat_as_parameters_009( _, a1, a2, a3, a4, a5, a6, a7, a8, a9,      ... ) a1 x1, a2 x2, a3 x3, a4 x4, a5 x5, a6 x6, a7 x7, a8 x8, a9 x9
#   define _sat_as_parameters_010( _, a1, a2, a3, a4, a5, a6, a7, a8, a9, a10, ... ) a1 x1, a2 x2, a3 x3, a4 x4, a5 x5, a6 x6, a7 x7, a8 x8, a9 x9, a10 x10
#   define _sat_as_arguments_000(                                              ... ) 
#   define _sat_as_arguments_001(  _, a1,                                      ... ) x1
#   define _sat_as_arguments_002(  _, a1, a2,                                  ... ) x1, x2
#   define _sat_as_arguments_003(  _, a1, a2, a3,                              ... ) x1, x2, x3
#   define _sat_as_arguments_004(  _, a1, a2, a3, a4,                          ... ) x1, x2, x3, x4
#   define _sat_as_arguments_005(  _, a1, a2, a3, a4, a5,                      ... ) x1, x2, x3, x4, x5
#   define _sat_as_arguments_006(  _, a1, a2, a3, a4, a5, a6,                  ... ) x1, x2, x3, x4, x5, x6
#   define _sat_as_arguments_007(  _, a1, a2, a3, a4, a5, a6, a7,              ... ) x1, x2, x3, x4, x5, x6, x7
#   define _sat_as_arguments_008(  _, a1, a2, a3, a4, a5, a6, a7, a8,          ... ) x1, x2, x3, x4, x5, x6, x7, x8
#   define _sat_as_arguments_009(  _, a1, a2, a3, a4, a5, a6, a7, a8, a9,      ... ) x1, x2, x3, x4, x5, x6, x7, x8, x9
#   define _sat_as_arguments_010(  _, a1, a2, a3, a4, a5, a6, a7, a8, a9, a10, ... ) x1, x2, x3, x4, x5, x6, x7, x8, x9, x10

#endif

