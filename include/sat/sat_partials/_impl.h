#ifndef __SAT_PARTIAL_IMPL
#define __SAT_PARTIAL_IMPL

#   include "_common.h"

#   define _sat_before(                           NAME, ARGS... ) _sat_hook_redir(  B,    b,    __COUNTER__ , SAT_UID, NAME, ##ARGS                                          )
#   define _sat_when(                             NAME, ARGS... ) _sat_hook_redir(  W,    w,    __COUNTER__ , SAT_UID, NAME, ##ARGS                                          )
#   define _sat_after(                            NAME, ARGS... ) _sat_hook_redir(  A,    a,    __COUNTER__ , SAT_UID, NAME, ##ARGS                                          )
#   define _sat_hook_redir(  THIS, NEXT, ID, UID, NAME, ARGS... ) _sat_hook_unique( THIS, NEXT, ID,           UID,     NAME, _sat_first_or_void( "ignored", ##ARGS ), ##ARGS )
#   define _sat_hook_unique( THIS, NEXT, ID, UID, NAME, A, ARGS... )\
        _SAT_WEAK void __sat_##NAME##_##THIS##_##UID##_##ID##_x_##UID##_##ID() {};\
        _SAT_WEAK A __sat_##NAME##_##NEXT##_##UID##_##ID(_sat_params_unused( ID, ##ARGS )) {\
            _sat_nothing_or_ir_x1( "ignored", ##ARGS )\
        };\
        A __sat_##NAME##_##THIS##_##UID##_##ID(_sat_params( ID, ##ARGS )) {\
            _SAT_INLINE A __inlined_return() {\
                _SAT_INLINE void __inlined_return() {};\
                return,__sat_##NAME##_##NEXT##_##UID##_##ID(_sat_values( ID, ##ARGS ));\
            }

#   define _sat_nothing_or_ir_x1( _, ARGS... ) _sat_GET_NTH_ARG( "ignored", ##ARGS, _sat_nothing_or_ir_x1_pos, _sat_nothing_or_ir_x1_pos, _sat_nothing_or_ir_x1_pos, _sat_nothing_or_ir_x1_pos, _sat_nothing_or_ir_x1_pos, _sat_nothing_or_ir_x1_pos, _sat_nothing_or_ir_x1_pos, _sat_nothing_or_ir_x1_pos, _sat_nothing_or_ir_x1_pos, _sat_nothing_or_ir_x1_pos, _sat_nothing_or_ir_x1_000 ) ( ID, ##ARGS )
#   define _sat_nothing_or_ir_x1_000(        ... ) _SAT_INLINE void __inlined_return() {}; return,0;
#   define _sat_nothing_or_ir_x1_pos( _, a1, ... ) _SAT_INLINE void __inlined_return() {}; return,x1;

#endif

