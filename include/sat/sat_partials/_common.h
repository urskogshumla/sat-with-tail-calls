#ifndef __SAT_PARTIAL_COMMON
#define __SAT_PARTIAL_COMMON

#   define _SAT_WEAK   __attribute__((used)) __attribute__((weak))
#   define _SAT_INLINE __attribute__((always_inline)) inline
#   define _sat_GET_NTH_ARG( I, _10, _9, _8, _7, _6, _5, _4, _3, _2, _1, N, ...) N

#   define _sat_first_or_void( _, ARGS... ) _sat_GET_NTH_ARG( "ignored", ##ARGS, _sat_first_or_void_pos, _sat_first_or_void_pos, _sat_first_or_void_pos, _sat_first_or_void_pos, _sat_first_or_void_pos, _sat_first_or_void_pos, _sat_first_or_void_pos, _sat_first_or_void_pos, _sat_first_or_void_pos, _sat_first_or_void_pos, _sat_first_or_void_000 ) ( _, ##ARGS )
#   define _sat_params_unused( _, ARGS... ) _sat_GET_NTH_ARG( "ignored", ##ARGS, _sat_params_unused_010, _sat_is_odd,            _sat_params_unused_008, _sat_is_odd,            _sat_params_unused_006, _sat_is_odd,            _sat_params_unused_004, _sat_is_odd,            _sat_params_unused_002, _sat_is_odd,            _sat_params_unused_000 ) ( ID, ##ARGS )
#   define _sat_params(        _, ARGS... ) _sat_GET_NTH_ARG( "ignored", ##ARGS, _sat_params_010,        _sat_is_odd,            _sat_params_008,        _sat_is_odd,            _sat_params_006,        _sat_is_odd,            _sat_params_004,        _sat_is_odd,            _sat_params_002,        _sat_is_odd,            _sat_params_000        ) ( ID, ##ARGS )
#   define _sat_values(        _, ARGS... ) _sat_GET_NTH_ARG( "ignored", ##ARGS, _sat_values_010,        _sat_is_odd,            _sat_values_008,        _sat_is_odd,            _sat_values_006,        _sat_is_odd,            _sat_values_004,        _sat_is_odd,            _sat_values_002,        _sat_is_odd,            _sat_values_000        ) ( ID, ##ARGS )

#   define _sat_first_or_void_000(                                             ... ) int
#   define _sat_first_or_void_pos( _, a1,                                      ... ) a1
#   define _sat_params_unused_000( _,                                          ... ) void
#   define _sat_params_unused_002( _, A1, a1,                                  ... ) A1 x1
#   define _sat_params_unused_004( _, A1, a1, A2, a2,                          ... ) A1 x1, A2 x2
#   define _sat_params_unused_006( _, A1, a1, A2, a2, A3, a3,                  ... ) A1 x1, A2 x2, A3 x3
#   define _sat_params_unused_008( _, A1, a1, A2, a2, A3, a3, A4, a4,          ... ) A1 x1, A2 x2, A3 x3, A4 x4
#   define _sat_params_unused_010( _, A1, a1, A2, a2, A3, a3, A4, a4, A5, a5,  ... ) A1 x1, A2 x2, A3 x3, A4 x4, A5 x5
#   define _sat_params_000(                                                    ... ) void
#   define _sat_params_002(        _, A1, a1,                                  ... ) A1 a1
#   define _sat_params_004(        _, A1, a1, A2, a2,                          ... ) A1 a1, A2 a2
#   define _sat_params_006(        _, A1, a1, A2, a2, A3, a3,                  ... ) A1 a1, A2 a2, A3 a3
#   define _sat_params_008(        _, A1, a1, A2, a2, A3, a3, A4, a4,          ... ) A1 a1, A2 a2, A3 a3, A4 a4
#   define _sat_params_010(        _, A1, a1, A2, a2, A3, a3, A4, a4, A5, a5,  ... ) A1 a1, A2 a2, A3 a3, A4 a4, A5 a5
#   define _sat_values_000(                                                    ... )
#   define _sat_values_002(        _, A1, a1,                                  ... ) a1
#   define _sat_values_004(        _, A1, a1, A2, a2,                          ... ) a1, a2
#   define _sat_values_006(        _, A1, a1, A2, a2, A3, a3,                  ... ) a1, a2, a3
#   define _sat_values_008(        _, A1, a1, A2, a2, A3, a3, A4, a4,          ... ) a1, a2, a3, a4
#   define _sat_values_010(        _, A1, a1, A2, a2, A3, a3, A4, a4, A5, a5,  ... ) a1, a2, a3, a4, a5

#endif

