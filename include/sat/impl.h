#include "sat_partials/_guard_uid.c"

#ifndef __SAT_IMPL__
#define __SAT_IMPL__

#   ifdef __SAT_DECL__
#   include "nodecl.h"
#   endif

#   include "sat_partials/_impl.h"

#   define before( NAME, ARGS... ) _sat_before( NAME, ##ARGS )
#   define when(   NAME, ARGS... ) _sat_when(   NAME, ##ARGS )
#   define after(  NAME, ARGS... ) _sat_after(  NAME, ##ARGS )
#   define return                    return __inlined_return()

#endif

