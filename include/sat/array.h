#pragma once

// Basic interface

#define array_decl( NAME, TYPE ) _esa( NAME, TYPE )
#define array( NAME ) _esa_member( NAME )
#define array_foreach( NAME ) _esa_foreach( NAME )
#define array_at( NAME, INDEX ) _esa_at( NAME, INDEX )
#define array_length( NAME ) _esa_length( NAME )

// Non-cute interface

#define array_address( NAME ) (&__start_esa_##NAME )
#define array_type( NAME ) ( typeof(__start_esa_##NAME.x) )
#define array_container( NAME ) ( _esacontainer_##NAME )

// Implementation

#define   _esa( NAME, TYPE )      __esa( NAME, TYPE, __COUNTER__ )
#define  __esa( NAME, TYPE, ID ) ___esa( NAME, TYPE, ID )

#ifdef __MACH__
#define ___esa( NAME, TYPE, ID )\
    typedef struct { __attribute__((__aligned__)) TYPE x; } _esacontainer_##NAME;\
    __attribute__((__used__, __section__("__DATA,esa_" #NAME ), __aligned__)) static struct {} _esa_placeholder_##ID;\
    extern _esacontainer_##NAME __start_esa_##NAME, __stop_esa_##NAME;
#else
#define ___esa( NAME, TYPE, ID )\
    typedef struct { __attribute__((__aligned__)) TYPE x; } _esacontainer_##NAME;\
    __attribute__((__used__, __section__("esa_" #NAME ), __aligned__)) static struct {} _esa_placeholder_##ID;\
    extern _esacontainer_##NAME __start_esa_##NAME, __stop_esa_##NAME;
#endif

#define   _esa_member( NAME )      __esa_member( NAME, __COUNTER__ )
#define  __esa_member( NAME, ID ) ___esa_member( NAME, ID )

#ifdef __MACH__
#define ___esa_member( NAME, ID )\
    __attribute__((__used__, __section__("__DATA,esa_" #NAME ), __aligned__))\
    static _esacontainer_##NAME _esa_##NAME##_##ID  

#else
#define ___esa_member( NAME, ID )\
    __attribute__((__used__, __section__("esa_" #NAME ), __aligned__))\
    static _esacontainer_##NAME _esa_##NAME##_##ID  
#endif

#define _esa_foreach( NAME )\
    for\
    (\
        typeof(__start_esa_##NAME.x) *it = (typeof(__start_esa_##NAME.x) *)(&__start_esa_##NAME);\
        (_esacontainer_##NAME *)it < &__stop_esa_##NAME;\
        it = (typeof(__start_esa_##NAME.x) *)(((_esacontainer_##NAME *)it) + 1)\
    )

#define _esa_at( NAME, INDEX ) ( (typeof(__start_esa_##NAME.x))(((&__start_esa_##NAME) + INDEX)->x) )

#define _esa_length( NAME ) ((int)( &__stop_esa_##NAME - &__start_esa_##NAME ))

