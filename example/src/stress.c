#include <stdio.h>

#include <sat/decl.h>

sat(passing_some_strings, char *, char *, char *);
sat(handling_some_doubles, double, double, double);

#include <sat/impl.h>

when(program_running)
    printf("Doing stress test\n");
    passing_some_strings("A simple string", "short",
        "    A real string that even has\n"
        "    line breaks and paragraphs\n"
        "    \n"
        "    Look at it go!");
    return;
}

when(handling_some_doubles, double, a, double, b, double, c)
    printf("The doubles' sum is %lf.\n", a + b + c);
    return;
}

when(handling_some_doubles, double, a, double, b, double, c)
    printf("The doubles are %lf %lf and %lf.\n", a, b, c);
    return;
}

when(passing_some_strings, char *, a, char *, b, char *, c)
    printf(":: %s \\\\ %s ::\n", a, b);
    printf(">>>>>>>\n");
    printf("%s\n", c);
    printf("<<<<<<<\n");
    return;
}

when(passing_some_strings, char *, a, char *, b, char *, c)
    handling_some_doubles(3.1, 3.2, 3.6);
    return;
}

when(passing_some_strings, char *, a, char *, b, char *, c)
    printf("!! %s \\\\ %s !!\n", a, b);
    printf("]]]]]]]\n");
    printf("%s\n", c);
    printf("[[[[[[[\n");
    return;
}

