#include <sat/decl.h>

sat(program_handling_args, const int, const char **);
sat(program_running);
sat(program_exiting, int);
sat(not_used, float);

#include <stdio.h>

int main(const int argc, const char *argv[])
{
    program_handling_args(argc, argv);
    int result = program_running();
    not_used(43.0);
    return program_exiting(result);
}
