#include <stdio.h>

#include <sat/impl.h>

when(program_handling_args, const int, argc, const char **, argv)
    for (int i = 0; i < argc; i++)
        printf("    > %s\n", argv[i]);
    return;
}
