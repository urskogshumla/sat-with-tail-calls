#include <stdio.h>

#include <sat/decl.h>

sat(work, char);

#include <sat/impl.h>

when(program_running)
    printf("Working B...\n");
    work('B');
    printf("Done with B!\n");
    return;
}
when(program_running)
    printf("Working C...\n");
    work('C');
    printf("Done with C!\n");
    return;
}
when(program_running)
    printf("Working A...\n");
    work('A');
    printf("Done with A!\n");
    return;
}
