#include <stdio.h>

#include <sat/decl.h>

sat(getting_special_value, int);

#include <sat/impl.h>

when(program_running)
    printf("Special value for 10 is %d!\n", getting_special_value(10));
    return;
}
